const express = require("express");
const studentRoutes = require("./src/student/routes.cjs");

const app = express();
const port = process.env.PORT || 3000;

app.use(express.json());

app.get("/", (req, res)=>{
    console.log("server.app.get Inside");
    res.send(` || API routes ||
    - get "/api/v1/students/" -> show all students data ||
    - get "/:id" -> show particular student data ||
    - post "/" -> add new student data ||
    - delete "/:id" -> delete particular student data if exist ||
    - put "/:id" -> update particular student data ||
    `);
});

app.use("/api/v1/students", studentRoutes)

app.listen(port, ()=>{
    console.log("app listening on port ",port);
});