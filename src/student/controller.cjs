const pool = require("../../database.cjs");
const queries = require("../student/queries.cjs");

const getStudents = (req, res) => {
    pool.query(queries.getStudents, (error, results) => {
        if (error) {
            throw error;
        } else {
            res.status(200).json(results.rows);
        }
    });
};

const getStudentById = (req, res) => {

    const id = parseInt(req.params.id);
    pool.query(queries.getStudentById, [id], (error, results) => {
        if (error) {
            throw error;
        } else {
            if(results.rows.length == 1){
                res.status(200).json(results.rows);
            }else{
                res.status(404).send("Student does not exist in the database.");
            }
            
        }
    });
};

const addStudent = (req, res) => {
    const { name, email, age, dob } = req.body;

    //check if email exist
    pool.query(queries.checkEmailExists, [email], (error, results) => {
        if (error) {
            throw error;
        } else {
            if (results.rows.length) {
                res.send("Email already exists.");
            } else {
                pool.query(queries.addStudent,
                    [
                        name,
                        email,
                        age,
                        dob
                    ],
                    (error, results) => {
                        if (error) {
                            throw error;
                        } else {
                            res.status(201).send("Student created successfully!");
                        }
                    }
                );
            }
        }
    })

}

const removeStudent = (req, res) => {

    const id = parseInt(req.params.id);
    pool.query(queries.getStudentById, [id], (error, results) => {

        if (error) {
            throw error;
        } else {
            const noStudentFound = !results.rows.length;

            if (noStudentFound) {
                res.status(404).send("Student does not exist in the database");
            } else {
                pool.query(queries.removeStudent, [id], (error, results) => {
                    if (error) {
                        throw error;
                    } else {

                        res.status(200).send("Student deleted from the database");
                    }
                });
            }
        }
    });

};

const updateStudent = (req, res) => {
    const id = parseInt(req.params.id);
    const { name, email, age, dob } = req.body;
    pool.query(queries.getStudentById, [id], (error, results) => {

        if (error) {
            throw error;
        } else {
            const noStudentFound = !results.rows.length;

            if (noStudentFound) {
                res.send("Student does not exist in the database");
            } else {

                pool.query(queries.updateStudent, [
                    name,
                    id,
                ],
                    (error, results) => {
                        if(error){
                            throw error;
                        }else{
                            res.status(200).send("Student updated in the database");
                        }

                    });

            }
        }
    });
};

module.exports = {
    getStudents,
    getStudentById,
    addStudent,
    removeStudent,
    updateStudent,
};